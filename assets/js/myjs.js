$(document).ready(function() {
	new WOW().init();

	$('#normal-imglist3').utilCarousel({
		pagination : false,
		navigationText : ['<i class="fa  fa-angle-left"></i>', '<i class="fa  fa-angle-right"></i>'],
		navigation : true,
		autoPlay : true,
		interval : 5000,
		rewind : true,
		showItems : 1,
		swipe:false,
		drag:false,
		mouseWheel:false,
		scrollPerPage: false,
		pauseOnHover: false,
		breakPoints : [[200, 1]]
	});

	$(".navmobile").click(function(){
		$(".mainmenutop").slideToggle("fast");
	});


});






